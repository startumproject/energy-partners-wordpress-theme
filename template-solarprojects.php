<?php
/* Template Name: Solar Projects Page */ 

        if(is_page(1161)) { 
            get_header('solar'); 
            
        } else { 
            get_header(); 
            
        } wp_head(); 
       
?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">      
                    <div class="project-filters-divisions">
                         <h2>PROJECTS</h2> 
                        
                <?php echo'<div class="facet-filter-two">';
                        echo facetwp_display( 'facet', 'client' );
                            echo '</div>';?>
                <?php echo'<div class="facet-filter-three">'; 
                        echo facetwp_display( 'facet', 'industry' ); 
                            echo '</div>';?>  
                <?php echo'<div class="facet-filter-four">'; 
                        echo facetwp_display( 'facet', 'search' ); 
                            echo '</div>';?>
                        </div>
                                     
                   <div class="full-width-container">
                   <h3>Solar Projects</h3>
		<?php echo facetwp_display( 'template', 'new_solar_template' ); ?>
                    </div>
                    </div>                  
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
