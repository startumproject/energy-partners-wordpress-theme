<?php
/**
 * The header for Steam
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Energy_Partners
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'energypartners' ); ?></a>
        <div class="top-header-section intelligence-top-header">
            <span class="corporate-link"><a href="http://localhost/energypartners"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
EP Corporate</a></span>
            <span class="top-phone">Call Us <a href="tel:0861 377 770">0861 377 770</a></span>
        </div>

	<header id="masthead" class="site-header">
		<div class="site-branding">
                    <div class="site-branding-text">
                        <div class="intelligence-logo">
                            <a href="http://localhost/energypartners/steam"><img class="header-logo" src="http://localhost/energypartners/wp-content/uploads/2018/09/EPSteamLogo@2x.png" /></a>
                        </div>
                    </div>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Header Menu', 'energypartners' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'steam', 'menu_id' => 'steam-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
