<?php
/**
 * Template part for projects posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Energy_Partners
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php energypartners_post_thumbnail(); ?>
    
    
        <div class="projects-banner">
            <img src="<?php echo the_field('banner_image'); ?>" />
        </div>

        <div class="project-top">
                <div class="row">
                    <div class="col-md-6" id="project-title">
                        <h1><?php the_field('project_title'); ?></h1>
                    </div>
                    <div class="col-md-6" id="project-stat">
                        <h1><?php the_field('project_stat'); ?></h1>
                    </div>
                </div>
        </div>
        <div class="project-description">
                    <div class="row">
                        <div class="col-md-6" id="project-quote">
                            <h4><?php the_field('project_quote'); ?></h4>
                        </div>
                        <div class="col-md-6" id="project-technical">
                            <?php the_field('project_technical_information'); ?>
                        </div>
                    </div>
        </div>

        

            
        <!--Start Project Slider -->
<?php 

$images = get_field('project_slider');

if( $images ): ?>
    <div class="slider">
        <ul class="project-slider">
            <?php foreach( $images as $image ): ?>
                <li>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

<?php endif; ?>

        <!--End Project Slider -->
        
        
        <!--jquery for bx slider-->
           <script>
                $(document).ready(function(){
                  $('.project-slider').bxSlider({
                      mode: 'fade'
                  });
                });
              </script>
        <!--end jquery for slider -->
   
    
        <div class="full-width-container">
        <div class="project-info">
            <div class="row">
                <div class="col-md-12">
                    <p><strong>Project Background</strong></p>
                    <?php the_field('project_information'); ?>
                </div>
        </div>
        
    </div>
            <div class="related-posts-title"><h2>Related Projects </h2></div>
            <div class="related-posts-container">
                
                
                            <?php
                            $cat_search = array();
                            $cats = get_terms('utility');
                            
                            
                            if ( ! empty( $cats ) ) {                                
                                foreach ( $cats as $cat ) {
                                    $cat_search[] = $cat->term_id;
                                }
                            }
                            
                            if ( empty( $cat_search ) ) {
                                return;
                            }
                            
                            $args = array(
                                'post_type' => 'projects',
                                'posts_per_page' => 3,
                                'tax_query' => array(
                                    array(
                                        
                                        'taxonomy' => 'utility',
                                        'field' => 'term_id',
                                        'terms' => $cat_search
                                    )
                                )
                            );
                            
                            $related_projects = new WP_Query( $args );
                            
                            if ( $related_projects->have_posts() ) {
                                while ( $related_projects->have_posts() ) {
                                    $related_projects->the_post();
                                    ?>
                                    <div class="projects-single-container">
                                        <img src="<?php echo the_field('banner_image'); ?>" />
                                        <div class="project-single-info">
                                            <a href="<?php the_permalink(); ?>">
                                            <span class="project-excerpt"><?php echo the_field('project_excerpt');?></span><br>
                                            <span class="project-title"><?php echo the_field('project_title');?></span></a>
                                            
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            
                            wp_reset_postdata();
                            
                    ?>

            </div>
<!--            <div class="related-primary-container">
            <div class="related-taxonomy-container">
                //<?php                 
//                $client = wp_get_post_terms( $post->ID, 'client', array('fields' => 'all') );
//                $name = $client[0]->name;
//                $url = get_term_link($client[0]->term_id);
//                echo "<h2>More projects for <a href='http://localhost/energypartners/projects'>{$name}</a></h2>";
//                
//                ?>
                <a href='http://localhost/energypartners/projects'><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                
            </div>
            <hr>
        </div>-->
        </div>




</article><!-- #post-<?php the_ID(); ?> -->
