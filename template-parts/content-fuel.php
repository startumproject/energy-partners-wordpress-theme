<?php
/**
 * Template part for displaying page content in template-fuel.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Energy_Partners
 */

?>

<style type="text/css">

.acf-map {
	width: 100%;
	height: 200px;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPOhsVk5OiCdNgV2Sn1Q-PdR6gSDEkUKI"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>

          <div class="top-page-container" id="fuel-header">
            <div class="full-width-container">
                <div class="row">
                    <div class="col-md-6 col-lg-6" id="division-title">
                        <h1><?php the_field('header_title_'); ?></h1>
                       </div> 
                    <div class="col-md-6 col-lg-6" id="division-description">
                        <h4><?php the_field('header_description'); ?><a href="<?php the_field('header_link'); ?>"> Read more</a></h4> 
                      
                    </div>
                 </div>
            </div>
        </div>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="division-head-container" style="background-image:url('<?php echo the_field('division_banner_image'); ?>');">
        <div class="division-stat">
            <?php the_field('banner_stat'); ?>
        </div>
    </div>
    
    <div class="full-width-container text-section">
        <h3>Solutions & Services</h3>
        <h4><?php the_field('division_solutions_and_services'); ?></h4>
    </div>
 
    <div class="alternate-full-width-image">
        <img src="<?php echo the_field('alternate_full_width_image'); ?>">
    </div>
    
    <div class="home-map">
    <?php 

                            $location = get_field('map');

                            if( !empty($location) ):
                            ?>
                            <div class="acf-map">
                                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                            </div>
                            <?php endif; ?>
</div>
    
<div class="layout-3 home">
                            <div class="row">
                                <div class="col-md-6 col-lg-6" id="left-box">
                                    <div class="left-box-content">
				<?php echo the_field('location_one'); ?>
                                
                                <div class="col-md-6 col-lg-6">
                                <?php echo the_field('location_two'); ?>
                                                                        
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <?php echo the_field('location_three'); ?>
                                </div>
                                    </div>                        
                                </div>
                                <div class="col-md-6 col-lg-6" id="right-box">
                                    <div class="right-box-content">
				<p><?php 
                                        $form_object = get_field('gravity_form');
                                        gravity_form_enqueue_scripts($form_object['id'], true);
                                        gravity_form($form_object['id'], true, true, false, '', true, 1); 
                                    ?></p>
                                </div>
                                </div>
                            </div>
                        </div>
<!--footer section-->
    
	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'energypartners' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
