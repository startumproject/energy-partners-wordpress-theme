<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Energy_Partners
 */

?>

	</div><!-- #content -->
        <div class="footer-container">
	<footer id="colophon" class="site-footer">
		<div class="row">
                    <div class="col-sm-2"><?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-1') ) ?></div>
                    <div class="col-sm-2"><?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-2') ) ?></div>
                    <div class="col-sm-2"><?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-3') ) ?></div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2" id="last-column"><?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-6') ) ?></div>
                </div>
	</footer><!-- #colophon -->
        </div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
