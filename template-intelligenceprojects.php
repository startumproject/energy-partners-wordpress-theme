<?php
/* Template Name: Intelligence Projects Page */ 

        if(is_page(1572)) { 
            get_header('intelligence'); 
            
        } else { 
            get_header(); 
            
        } wp_head(); 
       
?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">      
                    <div class="project-filters-divisions">
                         <h2>PROJECTS</h2> 
                        
                <?php echo'<div class="facet-filter-two">';
                        echo facetwp_display( 'facet', 'client' );
                            echo '</div>';?>
                <?php echo'<div class="facet-filter-three">'; 
                        echo facetwp_display( 'facet', 'industry' ); 
                            echo '</div>';?>  
                <?php echo'<div class="facet-filter-four">'; 
                        echo facetwp_display( 'facet', 'search' ); 
                            echo '</div>';?>
                        </div>
                                     
                   <div class="full-width-container">
                   <h3>Intelligence Projects</h3>
		<?php echo facetwp_display( 'template', 'intelligence_projects' ); ?>
                    </div>
                    </div>                  
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
