<?php
/**
 * Energy Partners functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Energy_Partners
 */

if ( ! function_exists( 'energypartners_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function energypartners_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Energy Partners, use a find and replace
		 * to change 'energypartners' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'energypartners', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
                register_nav_menus( array(
                        'header' => esc_html__( 'Header', 'energypartners' ),
                ) );
                
                register_nav_menus( array(
                        'intelligence' => esc_html__( 'Intelligence', 'energypartners' ),
                ) );
                
                register_nav_menus( array(
                        'refrigeration' => esc_html__( 'Refrigeration', 'energypartners' ),
                ) );
                
                register_nav_menus( array(
                        'solar' => esc_html__( 'Solar', 'energypartners' ),
                ) );
                
                register_nav_menus( array(
                        'steam' => esc_html__( 'Steam', 'energypartners' ),
                ) );
                
                register_nav_menus( array(
                        'engineering' => esc_html__( 'Engineering', 'energypartners' ),
                ) );
                
                register_nav_menus( array(
                        'fuel' => esc_html__( 'Fuel', 'energypartners' ),
                ) );
                
                register_nav_menus( array(
                        'water' => esc_html__( 'Water', 'energypartners' ),
                ) );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'energypartners_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		//Add theme support for Custom logo
                    add_theme_support( 'custom-logo', array(
                        'width' => 90,
                        'height' => 90,
                        'flex-width' => true
                    ));
	}
endif;
add_action( 'after_setup_theme', 'energypartners_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function energypartners_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'energypartners_content_width', 640 );
}
add_action( 'after_setup_theme', 'energypartners_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function energypartners_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'energypartners' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Footer-1', 'energypartners' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h6 class="footer-widget-title">',
		'after_title'   => '</h6>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Footer-2', 'energypartners' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h6 class="footer-widget-title">',
		'after_title'   => '</h6>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Footer-3', 'energypartners' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h6 class="footer-widget-title">',
		'after_title'   => '</h6>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Footer-4', 'energypartners' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h6 class="footer-widget-title">',
		'after_title'   => '</h6>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Footer-5', 'energypartners' ),
		'id'            => 'footer-5',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h6 class="footer-widget-title">',
		'after_title'   => '</h6>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Footer-6', 'energypartners' ),
		'id'            => 'footer-6',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h6 class="footer-widget-title">',
		'after_title'   => '</h6>',
	) );
                register_sidebar( array(
		'name'          => esc_html__( 'Projects Section', 'energypartners' ),
		'id'            => 'projects-section-widget',
		'description'   => esc_html__( 'Add widgets here.', 'energypartners' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h6 class="project-section-widget-title">',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'energypartners_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function energypartners_scripts() {
            wp_enqueue_style( 'energypartners-bxslidercss', 'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css' );
            wp_enqueue_style( 'energypartners-fontawesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
            wp_enqueue_style( 'energypartners-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
            wp_enqueue_style( 'energypartners-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i' );         
            wp_enqueue_style( 'energypartners-style', get_stylesheet_uri() );
            wp_enqueue_script( 'energypartners-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20151215', true );        
            wp_localize_script( 'energypartners-navigation', 'energypartnersScreenReaderText', array(
                'expand' => __( 'Expand child menu', 'energypartners'),
                'collapse' => __( 'Collapse child menu', 'energypartners'),
            ) );
            wp_enqueue_script( 'energypartners-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
            wp_enqueue_script( 'energypartners-bxsliderjquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js');
            wp_enqueue_script( 'energypartners-bxsliderjs', 'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js');
            wp_enqueue_script( 'energypartners-bootstrapjs', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');

            
            
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'energypartners_scripts' );

//Add search bar to top menu

add_filter( 'wp_nav_menu_items','add_search_box', 10, 2 );
function add_search_box( $items, $args ) {
    $items .= '<li>' . get_search_form( false ) . '</li>';
    return $items;
}

//Google Map API Function

function my_acf_init() {
  acf_update_setting('google_api_key', 'AIzaSyDuyTNTNuYLIPvqg9FOuZ_FYeE0zY07TkA');
}
add_action('acf/init', 'my_acf_init');


//Add Gravity Forms to ACF Field
include_once('Gravity-Forms-ACF-Field-master/acf-gravity_forms.php');

//Add Woocommerce
add_theme_support( 'woocommerce' );

//Excerpt function

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

//Custom Post Type Projects
/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Projects', 'Post Type General Name', 'energypartners' ),
        'singular_name'       => _x( 'Project', 'Post Type Singular Name', 'energypartners' ),
        'menu_name'           => __( 'Projects', 'energypartners' ),
        'parent_item_colon'   => __( 'Parent Project', 'energypartners' ),
        'all_items'           => __( 'All Projects', 'energypartners' ),
        'view_item'           => __( 'View Project', 'energypartners' ),
        'add_new_item'        => __( 'Add New Project', 'energypartners' ),
        'add_new'             => __( 'Add New', 'energypartners' ),
        'edit_item'           => __( 'Edit Project', 'energypartners' ),
        'update_item'         => __( 'Update Project', 'energypartners' ),
        'search_items'        => __( 'Search Project', 'energypartners' ),
        'not_found'           => __( 'Not Found', 'energypartners' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'energypartners' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Project', 'energypartners' ),
        'description'         => __( 'Case Studies from previous and current Projects', 'energypartners' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'projects' ),
        'taxonomies'  => array( 'category' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'projects', $args );
 
}

// hook into the init action and call create_project_taxonomies when it fires
add_action( 'init', 'create_project_taxonomies', 0 );

// create two taxonomies
function create_project_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Clients', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Client', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Clients', 'textdomain' ),
		'all_items'         => __( 'All Clients', 'textdomain' ),
		'parent_item'       => __( 'Parent Client', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Client:', 'textdomain' ),
		'edit_item'         => __( 'Edit Client', 'textdomain' ),
		'update_item'       => __( 'Update Client', 'textdomain' ),
		'add_new_item'      => __( 'Add New Client', 'textdomain' ),
		'new_item_name'     => __( 'New Client Name', 'textdomain' ),
		'menu_name'         => __( 'Client', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'client' ),
	);

	register_taxonomy( 'client', array( 'projects' ), $args );

	// Add new taxonomy
	$labels = array(
		'name'              => _x( 'Industries', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Industry', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Industries', 'textdomain' ),
		'all_items'         => __( 'All Industries', 'textdomain' ),
		'parent_item'       => __( 'Parent Industry', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Industry:', 'textdomain' ),
		'edit_item'         => __( 'Edit Industry', 'textdomain' ),
		'update_item'       => __( 'Update Industry', 'textdomain' ),
		'add_new_item'      => __( 'Add New Industry', 'textdomain' ),
		'new_item_name'     => __( 'New Industry Name', 'textdomain' ),
		'menu_name'         => __( 'Industry', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'industry' ),
	);

	register_taxonomy( 'industry', array( 'projects' ), $args );
        
        	// Add new taxonomy
	$labels = array(
		'name'              => _x( 'Utilities', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Utility', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Utilities', 'textdomain' ),
		'all_items'         => __( 'All Utilities', 'textdomain' ),
		'parent_item'       => __( 'Parent Utility', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Utility:', 'textdomain' ),
		'edit_item'         => __( 'Edit Utility', 'textdomain' ),
		'update_item'       => __( 'Update Utility', 'textdomain' ),
		'add_new_item'      => __( 'Add New Utility', 'textdomain' ),
		'new_item_name'     => __( 'New Utility Name', 'textdomain' ),
		'menu_name'         => __( 'Utility', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'utility' ),
	);

	register_taxonomy( 'utility', array( 'projects' ), $args );
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );

/*********************************/
/* Change Search Button Text
/**************************************/

// Add to your child-theme functions.php
add_filter('get_search_form', 'my_search_form_text');
 
function my_search_form_text($text) {
     $text = str_replace('value="Search ..."', 'value="Search"', $text); //set as value the text you want
     return $text;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load SVG Icon functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * WooCommerce
 */
/**
 * Remove related products output
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );










