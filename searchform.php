<?php
/**
 * Template for displaying search forms in Energy Partners
 *
 * @package WordPress
 * @subpackage energypartners
 * @since Energy Partners 1.0
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'energypartners' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', 'energypartners' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	
</form>