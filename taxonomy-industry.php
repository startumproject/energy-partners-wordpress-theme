<?php
/**
 * The template for displaying archive industry pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Energy_Partners
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
                    <div class="full-width-container">   
                        
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				
				the_archive_description( '<div class="archive-description-title">', '</div>' );
				?>
			</header><!-- .page-header -->
                        <div class="archive-projects-container">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/contentprojects', get_post_type( 'industry' ) );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
                    </div>
                    </div>
                  
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
