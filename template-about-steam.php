<?php 
/* Template Name: Steam About Page */

        if(is_page(740)) { 
            get_header('steam'); 
            
        } else { 
            get_header(); 
            
        } wp_head(); 
       
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
<?php 
    
if( have_rows('flexible_content_about') ):

    while( have_rows('flexible_content_about') ) : the_row();
		
		// get layout
		$layout = get_row_layout();
		
		
		// layout_1
		if( $layout === 'top_banner' ): ?>
                    
                <div class="top-page-container about">    
                <div class="full-width-container">
                    <div class="row">
			<div class="col-md-6 col-lg-6">
                                <h1><?php the_sub_field('left_column'); ?></h1>
                        </div>
                        <div class="col-md-6 col-lg-6" id="right-column-1">
                            <a class="button" href="<?php the_sub_field('right_column'); ?>" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>
DOWNLOAD COMPANY PROFILE</a>
			</div>
                    </div>
                </div>
                </div>

		<?php // layout_2	
		elseif( $layout === 'stats_box' ): ?>
			
			<div class="about-layout-2">
                    <div class="stat-container" style="background-image:url('<?php echo the_sub_field('stats_box_background_image'); ?>');">
                            <div class="full-width-container">
                                <div class="box">
                                    <h2><?php the_sub_field('box_one_numbers'); ?></h2>
                                    <span><?php the_sub_field('box_one_content'); ?></span></div>
                                <div class="box"><h2>
                                    <?php the_sub_field('box_two_numbers_'); ?></h2>
                                    <span><?php the_sub_field('box_two_content'); ?></span></div>
                                <div class="box">
                                    <h2><?php the_sub_field('box_three_numbers'); ?></h2>
                                    <span><?php the_sub_field('box_three_content'); ?></span></div>
                            </div>
                    </div>
			</div>
                    
               <?php // layout_3	
		elseif( $layout === 'two_wide_grid' ): ?>
			
			<div class="about-layout-3">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="about-wysiwyg-left"><?php the_sub_field('left_content_box'); ?></div>
                                </div>
                                
                                <div class="col-md-6 col-lg-6">                            
                                <img class="block-image" src="<?php echo the_sub_field('right_image_box'); ?>" />
                                </div>
                                
                            </div>
			</div>
                    
                <?php //layout 4
                elseif( $layout === 'solutions_&_services_container' ): ?>
			<div class="about-layout-4">
                            <div class="full-width-container">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-icon"><img src="<?php echo the_sub_field('icon'); ?>" /></div>
                                        <div class="solution-content"><span><?php echo the_sub_field('title'); ?></span><p><?php echo the_sub_field('content'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-icon"><img src="<?php echo the_sub_field('icon_2'); ?>" /></div>
                                        <div class="solution-content"><span><?php echo the_sub_field('title_2'); ?></span><p><?php echo the_sub_field('content_2'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-icon"><img src="<?php echo the_sub_field('icon_3'); ?>" /></div>
                                        <div class="solution-content"><span><?php echo the_sub_field('title_3'); ?></span><p><?php echo the_sub_field('content_3'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-icon"><img src="<?php echo the_sub_field('icon_4'); ?>" /></div>
                                        <div class="solution-content"><span><?php echo the_sub_field('title_4'); ?></span><p><?php echo the_sub_field('content_4'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-icon"><img src="<?php echo the_sub_field('icon_5'); ?>" /></div>
                                        <div class="solution-content"><span><?php echo the_sub_field('title_5'); ?></span><p><?php echo the_sub_field('content_5'); ?></p></div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>                    
                        </div>
                    
                <?php //layout 5
                elseif( $layout === 'board_of_directors' ): ?>
			<div class="about-layout-5">
                            
                            <div class="full-width-container">
                                <div class="row directors-title">
                                    <div class="col-md-8"><?php echo the_sub_field('title_information'); ?></div>
                                    <div class="col-md-4 logo"><img src="<?php echo the_sub_field('title_logo'); ?>"></div>
                                </div>                                  
                                
                                <div class="row">
                                    <div class="col-md-3"><img src="<?php echo the_sub_field('image'); ?>"></div>
                                    <div class="col-md-3"><?php echo the_sub_field('content'); ?></div>
                                    <div class="col-md-3"><img src="<?php echo the_sub_field('image_two'); ?>"></div>
                                    <div class="col-md-3"><?php echo the_sub_field('content_two'); ?></div>
                                </div>
                                
                                <div class="board-container">
                                    <div class="col"><img src="<?php echo the_sub_field('image_three'); ?>"><?php echo the_sub_field('content_three'); ?></div>
                                    <div class="col"><img src="<?php echo the_sub_field('image_four'); ?>"><?php echo the_sub_field('content_four'); ?></div>
                                    <div class="col"><img src="<?php echo the_sub_field('image_five'); ?>"><?php echo the_sub_field('content_five'); ?></div>
                                    <div class="col"><img src="<?php echo the_sub_field('image_six'); ?>"><?php echo the_sub_field('content_six'); ?></div>
                                    <div class="col"><img src="<?php echo the_sub_field('image_seven'); ?>"><?php echo the_sub_field('content_seven'); ?></div>
                                </div>
                                
                            </div>
                            
                        </div>
                    <?php //layout 6
                    elseif( $layout === 'logo_columns' ): ?>
			<div class="about-layout-6">
                                <div class="full-width-container logo-container">
                                    <h3>Our Clients</h3>
                                    <div class="row">
                                        <div class="col-md-12" id="industrial-column">
                                            <span>INDUSTRIAL</span>
                                            <?php 

                                            $images = get_sub_field('logo_column_one');
                                            $size = 'full'; // (thumbnail, medium, large, full or custom size)

                                            if( $images ): ?>
                                                <ul>
                                                    <?php foreach( $images as $image ): ?>
                                                        <li>
                                                            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php endif; ?></div>
                                    </div>
                                    </div>
                            </div>
            
<!--            End of flexible content layout statement-->
            
		<?php endif;

    endwhile;

endif;

?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();

