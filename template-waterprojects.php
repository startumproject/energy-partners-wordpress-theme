<?php
/* Template Name: Water Projects Page */ 

        if(is_page(1568)) { 
            get_header('water'); 
            
        } else { 
            get_header(); 
            
        } wp_head(); 
       
?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">      
                    <div class="project-filters-divisions">
                         <h2>PROJECTS</h2> 
                        
                <?php echo'<div class="facet-filter-two">';
                        echo facetwp_display( 'facet', 'client' );
                            echo '</div>';?>
                <?php echo'<div class="facet-filter-three">'; 
                        echo facetwp_display( 'facet', 'industry' ); 
                            echo '</div>';?>  
                <?php echo'<div class="facet-filter-four">'; 
                        echo facetwp_display( 'facet', 'search' ); 
                            echo '</div>';?>
                        </div>
                                     
                   <div class="full-width-container">
                   <h3>Water Projects</h3>
		<?php echo facetwp_display( 'template', 'water_projects' ); ?>
                    </div>
                    </div>                  
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
