<?php
/**
 * The template for displaying all woocommerce pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Energy_Partners
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
                    <div class="full-width-container">
                        <div class="woocommerce-content">

		<?php woocommerce_content(); ?>
                        </div>
                    </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
