<?php 
/* Template Name: Contact Page */

get_header();

?>
<style type="text/css">

.acf-map {
	width: 100%;
	height: 200px;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPOhsVk5OiCdNgV2Sn1Q-PdR6gSDEkUKI"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
<?php 
    
if( have_rows('flexible_contact_contact') ):

    while( have_rows('flexible_contact_contact') ) : the_row();
		
		// get layout
		$layout = get_row_layout();
		
		
		// layout_1
		if( $layout === 'top_banner' ): ?>
                <div class="top-page-container contact">    
                <div class="full-width-container">
                    <div class="row">
			<div class="col-md-6 col-lg-6">
                                <h1><?php the_sub_field('left_column'); ?></h1>
                        </div>
                        <div class="col-md-6 col-lg-6" id="right-column-1">
                            <h4><?php the_sub_field('right_column'); ?></h4>

			</div>
                    </div>
                </div>
                </div>

		<?php // layout_2	
		elseif( $layout === 'google_map' ): ?>
                    
                            <?php 

                            $location = get_sub_field('contact_map');

                            if( !empty($location) ):
                            ?>
                            <div class="acf-map">
                                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                            </div>
                            <?php endif; ?>
                    
                    
                    
                    
               <?php // layout_3	
		elseif( $layout === 'contact_details' ): ?>
			
			<div class="layout-3 contact">
                            <div class="row">
                                <div class="col-md-6 col-lg-6" id="left-box">
				<h3><?php the_sub_field('location_title'); ?></h3>
                                <p><i class="fa fa-phone" aria-hidden="true"></i><?php the_sub_field('phone_number'); ?></p>
                                <p><?php the_sub_field('address'); ?></p>
                                <p><i class="fa fa-envelope" aria-hidden="true"></i><?php the_sub_field('email_address'); ?></p>
                                <p><?php the_sub_field('company_registration_&_vat_details'); ?></p>
                                
                                <div class="col-md-6 col-lg-6">
                                    <h6><?php the_sub_field('location_two_title'); ?></h6>
                                    <p><i class="fa fa-phone" aria-hidden="true"></i><?php the_sub_field('location_two_phone_number'); ?></p>
                                    <p><?php the_sub_field('location_two_additional_details'); ?></p>                                    
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <h6><?php the_sub_field('location_three_title_'); ?></h6>
                                    <p><i class="fa fa-phone" aria-hidden="true"></i><?php the_sub_field('location_three_phone_number'); ?></p>
                                    <p><?php the_sub_field('location_three_additional_details'); ?></p>  
                                </div>
                                
                                </div>
                                <div class="col-md-6 col-lg-6" id="right-box">
				<p><?php 
                                $form_object = get_field('gravity_form');
                                echo do_shortcode('[gravityform id="1' . $form_object['id'] . '" title="true" description="true" ajax="true"]');
                            ?></p>
                                </div>
                            </div>
                        </div>
                    
                <?php //layout 4
                elseif( $layout === 'division_blocks' ): ?>
                    <div class="layout-4 contact">
                        
                    <div class="full-width-container">
                        <h3><?php echo the_sub_field('main_title'); ?></h3>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                            <div class="solution-content"><span><?php echo the_sub_field('title'); ?></span><p><?php echo the_sub_field('content'); ?><br><?php echo the_sub_field('email'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-content"><span><?php echo the_sub_field('title_2'); ?></span><p><?php echo the_sub_field('content_2'); ?><br><?php echo the_sub_field('email_2'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                            <div class="solution-content"><span><?php echo the_sub_field('title_3'); ?></span><p><?php echo the_sub_field('content_3'); ?><br><?php echo the_sub_field('email_3'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-content"><span><?php echo the_sub_field('title_4'); ?></span><p><?php echo the_sub_field('content_4'); ?><br><?php echo the_sub_field('email_4'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-content"><span><?php echo the_sub_field('title_5'); ?></span><p><?php echo the_sub_field('content_5'); ?><br><?php echo the_sub_field('email_5'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">  
                                        <div class="solution-card">
                                        <div class="solution-content"><span><?php echo the_sub_field('title_6'); ?></span><p><?php echo the_sub_field('content_6'); ?><br><?php echo the_sub_field('email_6'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="solution-card">
                                        <div class="solution-content"><span><?php echo the_sub_field('title_7'); ?></span><p><?php echo the_sub_field('content_7'); ?><br><?php echo the_sub_field('email_7'); ?></p></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">      
                                        <div class="solution-card">
                                        <div class="solution-content"><span><?php echo the_sub_field('title_8'); ?></span><p><?php echo the_sub_field('content_8'); ?><br><?php echo the_sub_field('email_8'); ?></p></div>
                                        </div>
                                    </div>
                                </div>
        </div>
                    </div>
                    
                    
                <?php //layout 5
                elseif( $layout === 'button_column' ): ?>
			<div class="layout-5">
				<p><?php the_sub_field('buttom_column_one'); ?></p>
			</div>
                    
		<?php endif;

    endwhile;

endif;

?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();


