<?php 
/* Template Name: Steam Products Page */

        if(is_page(808)) { 
            get_header('steam'); 
            
        } else { 
            get_header(); 
            
        } wp_head(); 
       
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
<?php 
    
if( have_rows('flexible_content_products') ):

    while( have_rows('flexible_content_products') ) : the_row();
		
		// get layout
		$layout = get_row_layout();
		
		
		// layout_1
		if( $layout === 'top_banner' ): ?>
                <div class="top-page-container solutions">    
                <div class="full-width-container">
                    <div class="row">
			<div class="col-md-6 col-lg-6">
                            <div class="left-column-content">
                                <h1><?php the_sub_field('left_column'); ?></h1>
                        </div>
                        </div>
                        <div class="col-md-6 col-lg-6" id="right-column-1">
                            <div class="top_form" id="steam-products">
                                <p><?php the_sub_field('gravity_form_title'); ?></p>
                            <p><?php 
                                $form_object = get_field('gravity_form');
                                echo do_shortcode('[gravityform id="4' . $form_object['id'] . '" title="true" description="true" ajax="true"]');
                            ?></p>
                            </div>
			</div>
                    </div>
                </div>
                </div>

		<?php // layout_2	
		elseif( $layout === 'products_sections' ): ?>
                    <div class="product-section-container">
                        <div class="full-width-container">
                            <div class="product-section"> 
                                <h5>BOSCH BOILERS</h5>
                                <ul class="products">
                                    <?php
                                        $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'bosch-boilers' );
                                        $loop = new WP_Query( $args );
                                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                                                <li class="product">    
                                                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
                                                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>
                                                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
                                                        <h6><?php the_title(); ?></h6>
                                                        <span class="price"><?php echo $product->get_price_html(); ?></span>                    
                                                    </a>
                                                </li>
                                    <?php endwhile; ?>
                                    <?php wp_reset_query(); ?>
                                </ul><!--/.products-->
                                <a class="button" href="http://localhost/energypartners/wp-content/uploads/2018/09/BoschBoilers.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>DOWNLOAD BROCHURE</a>
                            </div>
                            
                            <div class="product-section"> 
                                <h5>DRYDEN BOILERS</h5>
                                <ul class="products">
                                    <?php
                                        $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'dryden-boilers' );
                                        $loop = new WP_Query( $args );
                                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                                                <li class="product">    
                                                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
                                                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>
                                                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
                                                        <h6><?php the_title(); ?></h6>
                                                        <span class="price"><?php echo $product->get_price_html(); ?></span>                    
                                                    </a>
                                                </li>
                                    <?php endwhile; ?>
                                    <?php wp_reset_query(); ?>
                                </ul><!--/.products-->
                                <a class="button" href="http://localhost/energypartners/wp-content/uploads/2018/09/DrydenBoiler.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>DOWNLOAD BROCHURE</a>
                            </div>
                            
                                                        <div class="product-section"> 
                                <h5>FERROLI BOILERS</h5>
                                <ul class="products">
                                    <?php
                                        $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'ferroli-boilers' );
                                        $loop = new WP_Query( $args );
                                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                                                <li class="product">    
                                                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
                                                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>
                                                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
                                                        <h6><?php the_title(); ?></h6>
                                                        <span class="price"><?php echo $product->get_price_html(); ?></span>                    
                                                    </a>
                                                </li>
                                    <?php endwhile; ?>
                                    <?php wp_reset_query(); ?>
                                </ul><!--/.products-->
                                <a class="button" href="http://localhost/energypartners/wp-content/uploads/2018/09/FerroliBoilers.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>DOWNLOAD BROCHURE</a>
                            </div>
                            
                            <div class="product-section"> 
                                <h5>BURNERS</h5>
                                <ul class="products">
                                    <?php
                                        $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => 'burners' );
                                        $loop = new WP_Query( $args );
                                        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                                                <li class="product">    
                                                    <a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
                                                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>
                                                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
                                                        <h6><?php the_title(); ?></h6>
                                                        <span class="price"><?php echo $product->get_price_html(); ?></span>                    
                                                    </a>
                                                </li>
                                    <?php endwhile; ?>
                                    <?php wp_reset_query(); ?>
                                </ul><!--/.products-->
                                <div class="product-button">
                                <p>We are the exclusive agent for the full range of CIB Unigas burners in Southern Africa.</p>
                                <a class="button" id="products" href="http://localhost/energypartners/wp-content/uploads/2018/09/UNIGAS.pdf" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>DOWNLOAD BROCHURE</a>
                                </div>
                            </div>
                            
                                                
		<?php endif;

    endwhile;

endif;

?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
