<?php
/**
 * The template for displaying archive client pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Energy_Partners
 */

get_header();
?>

<div id="main-content" class="main-content">
 
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
 
            <header class="archive-header">
                <h1 class="archive-title">
                    Hello
                    <?php post_type_archive_title(); ?>
                </h1>
            </header><!-- .archive-header -->
             <?php //start by fetching the terms for the client taxonomy
                $terms = get_terms( 'utility', array(
                    'orderby'    => 'count',
                    'hide_empty' => 0
                ) );
                ?>
            
            <?php
                // now run a query for each client family
                foreach( $terms as $term ) {

                    // Define the query
                    $args = array(
                        'post_type' => 'projects',
                        'client' => $term->slug
                    );
                    $query = new WP_Query( $args );

                    // output the term name in a heading tag                
                    echo'<h2>' . $term->name . '</h2>';

                    // output the post titles in a list
                    echo '<ul>';

                        // Start the Loop
                        while ( $query->have_posts() ) : $query->the_post(); ?>

                        <li class="project-listing" id="post-<?php the_ID(); ?>">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>

                        <?php endwhile;

                    echo '</ul>';

                    // use reset postdata to restore orginal query
                    wp_reset_postdata();
 
} ?>

        </div><!-- #content -->
    </div><!-- #primary -->
</div><!-- #main-content -->

<?php

get_footer();
