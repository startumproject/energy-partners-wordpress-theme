<?php 
/* Template Name: Refrigeration Solutions & Services Page */

        if(is_page(636)) { 
            get_header('refrigeration'); 
            
        } else { 
            get_header(); 
            
        } wp_head(); 
       
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
<?php 
    
if( have_rows('flexible_content_solutions_&_services') ):

    while( have_rows('flexible_content_solutions_&_services') ) : the_row();
		
		// get layout
		$layout = get_row_layout();
		
		
		// layout_1
		if( $layout === 'top_banner' ): ?>
                <div class="top-page-container solutions">    
                <div class="full-width-container">
                    <div class="row">
			<div class="col-md-6 col-lg-6">
                            <div class="left-column-content">
                                <h1><?php the_sub_field('left_column'); ?></h1>
                        </div>
                        </div>
                        <div class="col-md-6 col-lg-6" id="right-column-1">
                            <div class="top_form">
                                <p><?php the_sub_field('gravity_form_title'); ?></p>
                            <p><?php 
                                $form_object = get_field('gravity_form_2');
                                echo do_shortcode('[gravityform id="3' . $form_object['id'] . '" title="true" description="true" ajax="true"]');
                            ?></p>
                            </div>
			</div>
                    </div>
                </div>
                </div>

		<?php // layout_2	
		elseif( $layout === 'division_banner' ): ?>
                    <div class="division-banner" style="background-image:url('<?php echo the_sub_field('banner_image'); ?>');">>                        
			<div class="division-banner-one">
                            <div class="full-width-container">
                            <div class="row">
                                <div class="col-md-6 col-lg-6" id="left-box">
                                    <span><?php the_sub_field('left_block_one'); ?></span>
                                </div>
                                <div class="col-md-6 col-lg-6" id="right-box">
                                    <span><?php the_sub_field('right_block_one'); ?></span>
                                </div>
                            </div>
                            </div>
			</div>
                    </div>
               
                <?php //layout 3
                elseif( $layout === 'content_blocks' ): ?>
			<div class="division-content-blocks">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="left-blue-block">
                                        <?php the_sub_field('left_blue_block'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="right-grey-block">
                                        <?php the_sub_field('right_grey_block'); ?>
                                    </div>
                                </div>
                            </div>
			</div>
                    
                                <?php //layout 4
                elseif( $layout === 'image_and_content_blocks' ): ?>
			<div class="division-image-content-blocks">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="left-image-block">
                                       <img src="<?php echo the_sub_field('left_image_block'); ?>" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="right-grey-block">
                                        <?php the_sub_field('right_grey_block'); ?>
                                    </div>
                                </div>
                            </div>
			</div>
                    
                    <?php //layout 5
                elseif( $layout === 'image_and_content_blocks_two' ): ?>
			<div class="division-image-content-blocks-two">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="left-block">
                                        <?php the_sub_field('left_block'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="right-image-block">
                                       <img src="<?php echo the_sub_field('right_image_block'); ?>" />
                                    </div>
                                </div>
                            </div>
			</div>
                    <?php //layout 6
                elseif( $layout === 'image_and_content_blocks_blue' ): ?>
			<div class="division-image-content-blocks-blue">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="left-block">
                                        <?php the_sub_field('left_block'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="right-image-block">
                                       <img src="<?php echo the_sub_field('right_image_block'); ?>" />
                                    </div>
                                </div>
                            </div>
			</div>
                        <?php //layout 7
                elseif( $layout === 'image_and_content_blocks_grey' ): ?>
			<div class="division-image-content-blocks-grey">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="left-block">
                                        <?php the_sub_field('left_block'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="right-image-block">
                                       <img src="<?php echo the_sub_field('right_image_block'); ?>" />
                                    </div>
                                </div>
                            </div>
			</div>
                    <?php //layout 8
                elseif( $layout === 'image_and_content_blocks_white' ): ?>
			<div class="division-image-content-blocks-white">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="left-image-block">
                                       <img src="<?php echo the_sub_field('left_image_block'); ?>" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="right-white-block">
                                        <?php the_sub_field('right_white_block'); ?>
                                    </div>
                                </div>
                            </div>
			</div>
		<?php endif;
                

    endwhile;

endif;

?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
