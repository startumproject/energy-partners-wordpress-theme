<?php
/**
 * The template for displaying archive projects pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Energy_Partners
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">      
                    <div class="project-filters">
                         <h2>PROJECTS</h2>  
                <?php echo'<div class="facet-filter-one">';
                        echo facetwp_display( 'facet', 'utility' );
                            echo '</div>';?>
                        
                <?php echo'<div class="facet-filter-two">';
                        echo facetwp_display( 'facet', 'client' );
                            echo '</div>';?>
                <?php echo'<div class="facet-filter-three">'; 
                        echo facetwp_display( 'facet', 'industry' ); 
                            echo '</div>';?>  
                <?php echo'<div class="facet-filter-four">'; 
                        echo facetwp_display( 'facet', 'search' ); 
                            echo '</div>';?>
                        </div>
                                     
                   <div class="full-width-container">
                   <h3>ALL Projects</h3>
		<?php echo facetwp_display( 'template', 'all_projects_filter' ); ?>
                    </div>
                    </div>                  
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
