<?php 
/* Template Name: Funding Page */

get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
<?php 
    
if( have_rows('flexible_content_funding') ):

    while( have_rows('flexible_content_funding') ) : the_row();
		
		// get layout
		$layout = get_row_layout();
		
		
		// layout_1
		if( $layout === 'top_banner' ): ?>
                <div class="top-page-container funding">    
                <div class="full-width-container">
                    <div class="row">
			<div class="col-md-6 col-lg-6">
                            <div class="left-column-content">
                                <h1><?php the_sub_field('left_column'); ?></h1>
                        </div>
                        </div>
                        <div class="col-md-6 col-lg-6" id="right-column-1">
                            <div class="top_form">
                                <span>Contact Me About Funding</span>
                            <p><?php 
                                $form_object = get_field('gravity_form_2');
                                echo do_shortcode('[gravityform id="2' . $form_object['id'] . '" title="true" description="true" ajax="true"]');
                            ?></p>
                            </div>
			</div>
                    </div>
                </div>
                </div>

		<?php // layout_2	
		elseif( $layout === 'funding_info_section_one' ): ?>
                    <div class="funding-boxes-one">                        
			<div class="funding-one">
                            <div class="row">
                                <div class="col-md-6 col-lg-6" id="left-box">
                                    <p><?php the_sub_field('left_info_block_one'); ?></p>
                                </div>
                                <div class="col-md-6 col-lg-6" id="right-box">
                                    <p><?php the_sub_field('right_info_block_one'); ?></p>
                                </div>
                            </div>
			</div>
                    </div>
                    <div class="funding-boxes-two">                        
			<div class="funding-two">
                           
                               <div class="row">
                               <div class="col-md-6 col-lg-6" id="left-box">
                                    <p><?php the_sub_field('left_info_block_one_two'); ?></p>
                               </div>
                               <div class="col-md-6 col-lg-6" id="right-box">
                                    <p><?php the_sub_field('right_info_block_two'); ?></p>
                               </div>
                            </div>
			</div>
                    </div>
                    
               <?php // layout_3	
		elseif( $layout === 'second_content_editor' ): ?>
			
			<div class="layout-3">
				<p><?php the_sub_field('wysiwyg_editor'); ?></p>
			</div>
                    
                <?php //layout 4
                elseif( $layout === 'gallery' ): ?>
			<div class="layout-4">
				<?php 
                                        $images = get_sub_field('gallery');
                                        $size = 'full'; // (thumbnail, medium, large, full or custom size)
                                        if( $images ): ?>                                            <ul>
                                                <?php foreach( $images as $image ): ?>
                                                    <li>
                                                        <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
			</div>
                    
                    
                <?php //layout 5
                elseif( $layout === 'button_column' ): ?>
			<div class="layout-5">
				<p><?php the_sub_field('buttom_column_one'); ?></p>
			</div>
                    
		<?php endif;

    endwhile;

endif;

?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
